<?php
	require_once 'includes/DatabaseOperations.php';
	
	$response = array();
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (isset($_POST['doctor_id'])) {
			$doctor_id = intval($_POST['doctor_id']);
			$db = new DatabaseOperations();
			$response['error'] = false;
			$t_appointments = $db->getNumberOfTodayAppointments($doctor_id);
			$c_appointments = $db->getNumberOfComingAppointments($doctor_id);
			$response['todays_appointments'] = $t_appointments['todays_appointments'];
			$response['coming_appointments'] = $c_appointments['coming_appointments'];
		}
		else {
			$response['error'] = true;
			$response['message'] = "Query Error";
		}
		
	}
	else {
		$response['error'] =  true;
		$response['message'] = "Error in Query";
	}
	
	echo json_encode($response);
