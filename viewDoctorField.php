<?php
	require_once 'includes/DatabaseOperations.php';
	
	$response = array();
	if ($_SERVER['REQUEST_METHOD'] == 'POST'){
		if (isset($_POST['field_id'])) {
			$field = intval($_POST['field_id']);
			$db = new DatabaseOperations();
			$field = $db->getDoctorField($field);
			$response['error'] = false;
			$response['area'] = $field['area'];
		}
	}
	else {
		$response['error'] = true;
		$response['message'] = "Error in Query";
	}
	
	echo json_encode($response);