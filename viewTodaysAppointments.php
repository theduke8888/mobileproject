<?php
	require_once 'includes/DatabaseOperations.php';
	
	$response = array();
	if($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (isset($_POST['doctor_id'])) {
			$doctor_id = intval($_POST['doctor_id']);
			$db = new DatabaseOperations();
			$response = $db->getTodaysAppointments($doctor_id);
			if (empty($response)) {
				$response['empty'] = true;
			}
		}
		else  {
			$response['error'] = true;
			$response['message'] = "Error in Query";
		}
	}
	else {
		$response['error'] = true;
		$response['message'] = "Error in Request Method";
	}
	
	echo  json_encode(array("appointments" => $response));