<?php
	
	
	class DatabaseConnections {
		private $conn;
		
		function __construct() {
		}
		
		function connect() {
			require_once dirname(__FILE__).'/Constants.php';
			$this->conn = new mysqli(DB_HOST, DB_USER_NAME, DB_PASSWORD, DB_NAME);
			if (mysqli_connect_errno()) {
				echo "Error to connect the database " . mysqli_connect_errno();
			}
			else {
				return $this->conn;
			}
		}
	}