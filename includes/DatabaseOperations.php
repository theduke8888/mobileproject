<?php
	
	
	class DatabaseOperations
	{
		private $conn;
		
		function __construct()
		{
			require_once dirname(__FILE__).'/DatabaseConnections.php';
			$db = new DatabaseConnections();
			$this->conn = $db->connect();
			
		}
		
		public function userLogin($email) {
			$stmt= $this->conn->prepare("select id from doctors where email = ?");
			$stmt->bind_param("s", $email);
			$stmt->execute();
			$stmt->store_result();
			return $stmt->num_rows > 0;
		}
		
		public function getUserByEmail($email) {
			$stmt = $this->conn->prepare("select * from doctors where email = ?");
			$stmt->bind_param("s", $email);
			$stmt->execute();
			//$stmt->store_result();
			return $stmt->get_result()->fetch_assoc();
		}
		
		public function  getAllReservations($doctor_id) {
			$stmt = $this->conn->prepare("SELECT * FROM appointments where doctor_id = ?");
			//$stmt->bind_param("s", $doctor_id);
			$stmt->execute();
			$rows = array();
			$result = $stmt->get_result();
			while($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}
			$stmt->close();
			return $rows;
		}
	
		public function getAllDoctors() {
			$stmt = $this->conn->prepare("select * from fields join doctors on fields.id = doctors.field_id order by area ");
			$stmt->execute();
			$rows = array();
			$result = $stmt->get_result();
			while($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}
			return $rows;
		}
		
		public function createAppointment($name, $address, $contact, $dob, $job, $doa, $gender, $doctor_id) {
			$time_zone = new DateTimeZone('Asia/Manila');
			$created_at = new DateTime();
			$updated_at = new DateTime();
			$stmt = $this->conn->prepare("insert into appointments(name, address, contact, dob, job,
									doa, gender, doctor_id, created_at, updated_at) values(?,?,?,?,?,?,?,?,current_timestamp(),current_timestamp())");
			$stmt->bind_param("sssssssi", $name, $address, $contact, $dob, $job, $doa, $gender,
				   $doctor_id);
			if ($stmt->execute())
				return 1;
			else
				return 2;
		}
		
		public function  getDoctorField($field_id) {
			$stmt = $this->conn->prepare("select area from fields where id = ?");
			$stmt->bind_param("i", $field_id);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
			
		}
		
		public function getNumberOfTodayAppointments($doctor_id) {
			$stmt = $this->conn->prepare("set time_zone = 'Asia/Manila'");
			$stmt->execute();
			$stmt = $this->conn->prepare("select count(*) as todays_appointments from appointments where doctor_id = ? and doa = CURDATE()");
			$stmt->bind_param("i", $doctor_id);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}
		
		public function getNumberOfComingAppointments($doctor_id) {
			$stmt = $this->conn->prepare("set time_zone = 'Asia/Manila'");
			$stmt->execute();
			$stmt = $this->conn->prepare("select count(*) as coming_appointments from appointments where doctor_id = ? and doa > CURDATE()");
			$stmt->bind_param("i", $doctor_id);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}
		
		public function getTodaysAppointments($doctor_id) {
			$stmt = $this->conn->prepare("set time_zone = 'Asia/Manila'");
			$stmt->execute();
			$stmt = $this->conn->prepare("select * from appointments where doctor_id = ? and doa = CURDATE()");
			$stmt->bind_param("i", $doctor_id);
			$stmt->execute();
			$rows = array();
			$result = $stmt->get_result();
			while ($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}
			return $rows;
		}
		
		public function getComingAppointments($doctor_id) {
			$stmt = $this->conn->prepare("set time_zone = 'Asia/Manila'");
			$stmt->execute();
			$stmt = $this->conn->prepare("select * from appointments where doctor_id = ? and doa > CURDATE() order by doa asc");
			$stmt->bind_param("i", $doctor_id);
			$stmt->execute();
			$rows = array();
			$result = $stmt->get_result();
			while ($row = $result->fetch_assoc()) {
				$rows[] = $row;
			}
			return $rows;
		}
		
		public function getDate() {
			$stmt = $this->conn->prepare("set time_zone = 'Asia/Manila'");
			$stmt->execute();
			$stmt = $this->conn->prepare("select curdate() as todaytime");
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}
	}
	
	