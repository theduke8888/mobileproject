<?php
	require_once 'includes/DatabaseOperations.php';
	
	$response = array();
	$db = new DatabaseOperations();
	$response = $db->getAllDoctors();
	echo json_encode(array("list_doctors" => $response));
