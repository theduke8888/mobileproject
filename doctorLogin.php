<?php

	require_once 'includes/DatabaseOperations.php';
	
	$response = array();
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (empty($_POST['email']) || empty($_POST['password'])) {
			$response['error'] = true;
			$response['message'] = "All fields are required";
		}
		elseif (isset($_POST['email']) && isset($_POST['password'])) {
			$db = new DatabaseOperations();
			if ($db->userLogin($_POST['email'])) {
				$user = $db->getUserByEmail($_POST['email']);
				if (password_verify($_POST['password'], $user["password_digest"])) {
					$response['error'] = false;
					$response['id'] = $user['id'];
					$response['name'] = $user['name'];
					$response['email'] = $user['email'];
					$response['license'] = $user['license'];
					$response['contact'] = $user['contact'];
					$response['address'] =  $user['address'];
					$response['field_id'] = $user['field_id'];
				}
				else {
					$response['error'] = true;
					$response['message'] = "Login details are invalid...";
				}
				
			}
			else {
				$response['error'] = true;
				$response['message'] = "Login details are invalid...";
			}
		}
		else {
			$response['error'] = true;
			$response['message'] = "Required fields missing....";
			
		}
	}
	
	echo json_encode($response);