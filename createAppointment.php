<?php
	require_once 'includes/DatabaseOperations.php';
	
	$response = array();
	$name = $_POST['name'];
	$address = $_POST['address'];
	$contact = $_POST['contact'];
	$dob = $_POST['dob'];
	$job = $_POST['job'];
	$doa = $_POST['doa'];
	$gender = $_POST['gender'];
	$doctor_id =  intval($_POST['doctor_id']);
	if($_SERVER['REQUEST_METHOD'] == 'POST') {
		if (empty($name) || empty($address) || empty($contact) || empty($dob) || empty($job) || empty($doa) ||
			   empty($gender) || empty($doctor_id)) {
			$response['error'] = true;
			$response['message'] = "Every field is required";
		}
		elseif (isset($name) || isset($address) || isset($contact) || isset($dob) || isset($job) || isset($doa) ||
			   isset($gender) || isset($doctor_id)) {
			$db = new DatabaseOperations();
			$result = $db->createAppointment($name, $address, $contact, $dob, $job, $doa, $gender, $doctor_id);
			if ($result == 1) {
				$response['error'] = false;
				$response['message'] = "Appointment is created, Thank you.";
			}
			elseif ($result == 2) {
				$response['error'] = true;
				$response['message'] = "Error in Saving";
			}
		}
		
	}
	else {
		$response['error'] = true;
		$response['message'] = "Invalid Request";
	}
	
	echo json_encode($response);